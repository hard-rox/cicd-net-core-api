pipeline{
    agent any

    stages{
        stage('Job Start'){
            steps{
                echo "Started Job '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}"
                emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Started Job '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Started Job '${env}'"
                )
            }
        }

        stage('Git Pull'){
            steps{
                echo 'Git Pull executing...'
                git branch: 'main', credentialsId: '3d282468-a730-44f7-a043-8502a3014af5', url: 'https://gitlab.com/hard-rox/cicd-net-core-api.git'
            }
        }

        stage('Build'){
            steps{
                echo 'Building project...'
                bat 'dotnet build'
            }
        }

        stage('Test'){
            steps{
                echo 'Testing project...'
                bat 'dotnet test'
            }
        }

        stage('Deploy'){
            steps{
                echo 'Deploying project...'
                bat 'dotnet publish -c release -r linux-arm'
            }
        }
    }

    post {
        success {
            echo 'Job Succeded'
            emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Job Finished '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Job Finished '${env}'"
                )
        }

        failure {
            echo 'Job Failed'
            emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Job Failed '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Job Failed '${BUILD_URL}''",
                    attachLog: true
                )
        }
    }
}
