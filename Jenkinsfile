pipeline{
    agent any

    stages{
        stage('Job Start'){
            steps{
                echo "Started Job '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}"
                emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Started Job '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Started Job '${env}'"
                )
            }
        }

        stage('Clean Workspace'){
            steps{
                echo 'Removing old builds & cleaning workspace...'
                bat 'dotnet clean'
                dir ('src/WebApp/bin') {
                    deleteDir()
                }
                dir ('src/WebApp/obj') {
                    deleteDir()
                }
            }
        }

        stage('Restore'){
            steps{
                echo 'Restoring project dependencies...'
                bat 'dotnet restore'
            }
        }

        stage('Build'){
            steps{
                echo 'Building project...'
                bat 'dotnet build'
            }
        }

        stage('Test'){
            steps{
                echo 'Testing project...'
                bat 'dotnet test'
            }
        }

        stage('Dev Deploy'){
            when{
                branch 'dev'
            }
            steps{
                echo 'Deploying project to DEV env...'
                bat 'dotnet publish -c release'
                bat 'xcopy /E /H /Y "src\\WebApp\\bin\\Release\\net5.0\\publish\\*.*" "C:\\inetpub\\wwwroot\\cicd\\netcore-dev\\"'
            }
        }

        stage('UAT Deploy'){
            when{
                branch 'uat'
            }
            steps{
                echo 'Deploying project to UAT env...'
                bat 'dotnet publish -c release'
                bat 'xcopy /E /H /Y "src\\WebApp\\bin\\Release\\net5.0\\publish\\*.*" "C:\\inetpub\\wwwroot\\cicd\\netcore-uat\\"'
            }
        }

        stage('Production Deploy'){
            when{
                branch 'main'
            }
            steps{
                echo 'Deploying project to Production env...'
                bat 'dotnet publish -c release'
                bat 'xcopy /E /H /Y "src\\WebApp\\bin\\Release\\net5.0\\publish\\*.*" "C:\\inetpub\\wwwroot\\cicd\\netcore-prod\\"'
            }
        }
    }

    post {
        success {
            echo 'Job Succeded'
            emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Job Finished '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Job Finished '${env}'"
                )
        }

        failure {
            echo 'Job Failed'
            emailext(
                    to: '$DEFAULT_RECIPIENTS',
                    subject: "Job Failed '${env.JOB_NAME}', Build#${env.BUILD_NUMBER}",
                    body: "Job Failed '${BUILD_URL}''",
                    attachLog: true
                )
        }
    }
}
